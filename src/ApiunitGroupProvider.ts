import { Event, ProviderResult, TreeDataProvider, TreeItemCollapsibleState, Uri } from 'vscode';
import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import { IRequest, IResponse, Request as HTTPRequest, Response as HTTPResponse } from 'http-file';
import { OpenFile } from './commands/openFile';
import { CreateTest } from './commands/createTest';
import { RenameTest } from './commands/renameTest';
import { DeleteTest } from './commands/deleteTest';
import { DuplicateTest } from './commands/duplicateTest';
import { RefreshTree } from './commands/refreshTree';

export class ApiunitGroupView
{
    constructor(context: vscode.ExtensionContext) {
        const rootPath = vscode.workspace.workspaceFolders?.[0].uri;
        if (!rootPath) return;

        const provider = new ApiunitGroupProvider(rootPath);
        const view = vscode.window.createTreeView('apiunitGroups', { treeDataProvider: provider, canSelectMany: true });
        context.subscriptions.push(view);
        context.subscriptions.push(CreateTest.register());
        context.subscriptions.push(RenameTest.register());
        context.subscriptions.push(DeleteTest.register());
        context.subscriptions.push(DuplicateTest.register());
        context.subscriptions.push(RefreshTree.register(provider));
    }
}

export class ApiunitGroupProvider implements TreeDataProvider<TreeItem>
{
    static readonly viewId = 'apiunitGroups';

    private _onDidChangeTreeData = new vscode.EventEmitter<TreeItem | undefined>()
    readonly onDidChangeTreeData: Event<void | TreeItem | null | undefined> | undefined = this._onDidChangeTreeData.event

    // public items: vscode.TreeItem[] = [];

    constructor (private workspaceRoot: Uri) {}

    refresh(root?: TreeItem): void {
        this._onDidChangeTreeData.fire(root);
    }
  
    getTreeItem(element: TreeItem): TreeItem | Thenable<TreeItem>
    {
        element.provider = this;
        return element;
    }

    getChildren(element: TreeItem = new Scenario('default', path.join(this.workspaceRoot.path, 'apiunit'))): ProviderResult<TreeItem[]>
    {
        if ('getChildren' in element)
        {
            return element.getChildren();
        }
        return [];
    }
}

export class TreeItem extends vscode.TreeItem
{
    provider?: ApiunitGroupProvider;

    constructor(label: string, collapsibleState: TreeItemCollapsibleState = TreeItemCollapsibleState.Collapsed)
    {
        super(label, collapsibleState);
    }

    getChildren(): TreeItem[]
    {
        return [];
    }
}

class DefaultScenario extends TreeItem
{
    constructor(public scenario: Scenario, public path: string)
    {
        super('default', TreeItemCollapsibleState.Collapsed);
        this.resourceUri = Uri.file(this.path);
        this.contextValue = "scenario"
    }

    getChildren(): TreeItem[]
    {
        const requestsDir = path.join(this.path, 'requests');
        const responsesDir = path.join(this.path, 'responses');

        const requestsExists = fs.existsSync(requestsDir);
        const responsesExists = fs.existsSync(responsesDir);

        if (requestsExists && !responsesExists)
        {
            throw new Error("requests directory exists without responses");
            
        }
        else if (responsesExists && !requestsExists)
        {
            throw new Error("Responses directory exists without requests");
        }
        else if (requestsExists && responsesExists)
        {
            const testsMap = new Map<string, Test>();

            const requests = fs.readdirSync(requestsDir);
            const responses = fs.readdirSync(responsesDir);
            const fileNames = new Set([...requests, ...responses]);

            for (const fileName of fileNames)
            {
                const testName = fileName.replace(/\.(resp|req|msg)$/, '');
                const test = new Test(testName, this.path, this.scenario);
                testsMap.set(testName, test);
            }
            return Array.from(testsMap.values());
        }
        else
        {
            return [];
        }
    }
}

export class Scenario extends TreeItem
{
    constructor(name: string, public path: string)
    {
        super(name, TreeItemCollapsibleState.Collapsed);
        this.resourceUri = Uri.file(this.path);
        this.contextValue = "scenario";
    }

    getChildren(): TreeItem[]
    {
        console.log('getting children for', this.path);

        const contents = fs.readdirSync(this.path);
        const scenarios: Scenario[] = [];
        let defaultScenario: DefaultScenario|undefined;

        for (const content of contents)
        {
            if (/\./.test(content))
            {
                // ignore files
                continue
            }
            else if (content === 'requests' || content === 'responses')
            {
                defaultScenario ??= new DefaultScenario(this, this.path);
            }
            else
            {
                scenarios.push(new Scenario(content, path.join(this.path, content)));
            }
        }

        if (scenarios.length === 0)
        {
            return defaultScenario?.getChildren() || [];
        }
        else if (defaultScenario)
        {
            scenarios.push(defaultScenario);
        }

        return scenarios;
    }
}

export class Test extends TreeItem
{
    constructor(public name: string, private scenarioPath: string, public scenario: Scenario)
    {
        super(name, TreeItemCollapsibleState.Collapsed);
        this.contextValue = "test";
    }

    getChildren(): [Request, Response]
    {
        return [
            new Request(path.join(this.scenarioPath, 'requests', `${this.name}.req`)),
            new Response(path.join(this.scenarioPath, 'responses', `${this.name}.resp`))
        ];
    }

    async duplicate(name: string)
    {
        const children = this.getChildren();
        await Promise.all(children.map(file => file.duplicate(name)));
    }

    async rename(name: string)
    {
        const children = this.getChildren();
        await Promise.all(children.map(file => file.rename(name)));
        this.label = name;
        this.name = name;
    }

    async delete()
    {
        const children = this.getChildren();
        await Promise.all(children.map(file => file.delete()));
    }
}

abstract class FileItem extends TreeItem
{
    constructor(label: string, protected path: string, collapsibleState?: TreeItemCollapsibleState)
    {
        super(label, collapsibleState);
        this.resourceUri = Uri.file(path);
        this.command = new OpenFile(this.resourceUri);
    }

    private _data?: string;
    protected get data(): string
    {
        this._data ??= fs.readFileSync(this.path, 'utf-8');
        return this._data;
    }

    async duplicate(name: string)
    {
        const oldPath = this.resourceUri?.path;
        if (!oldPath) return;
        const newPath = oldPath.replace(/(?<=\/)[^/]+(?=\.\w+)/, name);
        await fs.promises.copyFile(oldPath, newPath);
    }

    async rename(name: string)
    {
        const oldPath = this.resourceUri?.path;
        if (!oldPath) return;
        const newPath = oldPath.replace(/(?<=\/)[^/]+(?=\.\w+)/, name);
        await fs.promises.rename(oldPath, newPath);
        this.label = name;
        this.resourceUri = Uri.file(newPath);
        this.command = new OpenFile(this.resourceUri);
    }

    async delete()
    {
        if (!this.resourceUri?.path) return;

        fs.promises.unlink(this.resourceUri.path);
    }
} 

class Response extends FileItem
{
    constructor(path: string)
    {
        super('response', path, TreeItemCollapsibleState.Collapsed);
    }

    private _response?: IResponse;
    private get response(): IResponse
    {
        this._response ??= HTTPResponse.parse(this.data);
        return this._response;
    }

    getChildren(): TreeItem[]
    {
        const children = [
            new TreeItem(`StatusCode: ${this.response.statusCode}`),
            new TreeItem(`Headers: ${JSON.stringify(this.response.headers)}`)
        ];
        if (this.response.body)
        {
            const body = JSON.stringify(JSON.parse(this.response.body));
            const bodyTreeItem = new TreeItem(`Body: ${body}`);
            bodyTreeItem.tooltip = this.response.body;
            children.push(bodyTreeItem);
        }
        return children;
    }
}

class Request extends FileItem
{
    constructor(path: string)
    {
        super('request', path, TreeItemCollapsibleState.Collapsed);
    }

    private _request?: IRequest;
    private get request(): IRequest
    {
        this._request ??= HTTPRequest.parse(fs.readFileSync(this.path, 'utf8'));
        return this._request;
    }

    getChildren(): TreeItem[]
    {
        const children = [
            new TreeItem(`Method: ${this.request.method}`),
            new TreeItem(`Path: ${this.request.path}`),
            new TreeItem(`Headers: ${JSON.stringify(this.request.headers)}`),
        ]
        if (this.request.body)
        {
            const body = JSON.stringify(JSON.parse(this.request.body));
            const bodyTreeItem = new TreeItem(`Body: ${body}`);
            bodyTreeItem.tooltip = this.request.body;
            children.push(bodyTreeItem);
        }

        return children;
    }
}
