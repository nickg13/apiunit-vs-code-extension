import * as vscode  from 'vscode';
import { Test } from '../ApiunitGroupProvider';

export class DuplicateTest implements vscode.Command
{
    static readonly title = 'Duplicate Test'
    static readonly command = 'apiunit-ide.duplicateTest';

    get title() { return DuplicateTest.title; }
    get command() { return DuplicateTest.command; }
    arguments: [Test];

    constructor(test: Test)
    {
        this.arguments = [test];
    }

    static register(): vscode.Disposable
    {
        return vscode.commands.registerCommand(DuplicateTest.command, DuplicateTest.handler);
    }

    static async handler(test: Test)
    {
        const testName = (await vscode.window.showInputBox({ prompt: 'Test Name', value: test.name }))?.trim();

        if (!testName) return;

        await test.duplicate(testName);

        test.provider?.refresh(test.scenario);
    }
}
