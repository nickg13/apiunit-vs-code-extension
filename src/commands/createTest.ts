import * as fs from 'fs';
import * as vscode  from 'vscode';
import { Scenario } from '../ApiunitGroupProvider';

export class CreateTest implements vscode.Command
{
    static readonly title = 'Create Test'
    static readonly command = 'apiunit-ide.createTest';

    get title() { return CreateTest.title; }
    get command() { return CreateTest.command; }
    arguments: [Scenario];

    constructor(scenario: Scenario)
    {
        this.arguments = [scenario];
    }

    static register(): vscode.Disposable
    {
        return vscode.commands.registerCommand(CreateTest.command, CreateTest.handler);
    }

    static async handler(scenario: Scenario)
    {
        const testName = await vscode.window.showInputBox({ prompt: 'Test Name' });

        if (!testName) return;

        await Promise.all([
            fs.promises.writeFile(`${scenario.path}/requests/${testName}.req`, ''),
            fs.promises.writeFile(`${scenario.path}/responses/${testName}.resp`, '')
        ]);

        scenario.provider?.refresh(scenario);
    }
}
