import * as vscode  from 'vscode';
import { Test } from '../ApiunitGroupProvider';

export class DeleteTest implements vscode.Command
{
    static readonly title = 'Delete Test'
    static readonly command = 'apiunit-ide.deleteTest';

    get title() { return DeleteTest.title; }
    get command() { return DeleteTest.command; }
    arguments: [Test];

    constructor(test: Test)
    {
        this.arguments = [test];
    }

    static register(): vscode.Disposable
    {
        return vscode.commands.registerCommand(DeleteTest.command, DeleteTest.handler);
    }

    static async handler(test: Test)
    {
        await test.delete();

        test.provider?.refresh(test.scenario);
    }
}
