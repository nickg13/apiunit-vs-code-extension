import * as vscode  from 'vscode';
import { ApiunitGroupProvider, TreeItem } from '../ApiunitGroupProvider';

export class RefreshTree implements vscode.Command
{
    static readonly title = 'Create Test'
    static readonly command = 'apiunit-ide.refreshTree';

    get title() { return RefreshTree.title; }
    get command() { return RefreshTree.command; }
    arguments: [ApiunitGroupProvider|TreeItem];

    constructor(root: ApiunitGroupProvider|TreeItem)
    {
        this.arguments = [root];
    }

    static register(root: ApiunitGroupProvider): vscode.Disposable
    {
        return vscode.commands.registerCommand(RefreshTree.command, RefreshTree.handler(root));
    }

    static handler(root: ApiunitGroupProvider)
    {
        return (item: TreeItem) => item ? item.provider.refresh(item) : root.refresh();
    }
}
