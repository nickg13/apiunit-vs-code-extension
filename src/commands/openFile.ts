import { Command, Uri } from "vscode";

export class OpenFile implements Command
{
    title: string = "Open";
    command: string = "vscode.open";
    arguments: [Uri];

    constructor(fileName: Uri)
    {
        this.arguments = [fileName];
    }
}
