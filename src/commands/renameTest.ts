import * as vscode  from 'vscode';
import { Test } from '../ApiunitGroupProvider';

export class RenameTest implements vscode.Command
{
    static readonly title = 'Rename Test'
    static readonly command = 'apiunit-ide.renameTest';

    get title() { return RenameTest.title; }
    get command() { return RenameTest.command; }
    arguments: [Test];

    constructor(test: Test)
    {
        this.arguments = [test];
    }

    static register(): vscode.Disposable
    {
        return vscode.commands.registerCommand(RenameTest.command, RenameTest.handler);
    }

    static async handler(test: Test)
    {
        const testName = (await vscode.window.showInputBox({ prompt: 'Test Name', value: test.name }))?.trim();

        if (!testName) return;

        await test.rename(testName);

        test.provider?.refresh(test.scenario);
    }
}
