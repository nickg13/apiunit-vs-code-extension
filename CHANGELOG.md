# Change Log

All notable changes to the "apiunit-ide" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

- Create apiunit tree view with scenarios and tests
- Add Create Test command to scenario view item context
- Add Rename Test command to scenario test item view
